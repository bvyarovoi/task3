#!/bin/bash

tag_file="tag"
files_to_track="main.go|Dockerfile|go.mod|go.sum|.helm"
tag_content=$(cat "$tag_file")

if $(git diff --cached --name-status | grep -qE "${files_to_track}"); then
  set -e
  new_tag_content=$((tag_content + 1))
  echo "updated tag to $new_tag_content"
  docker build -t localhost:5000/app:$new_tag_content app
  docker push localhost:5000/app:$new_tag_content
  echo "$new_tag_content" > "$tag_file"
  git add "$tag_file"
  set +e

  if helm get values app 2>/dev/null 1>/dev/null ; then 
    helm upgrade --install app .helm --reset-values --set image.tag=$new_tag_content
  fi

fi

exit 0