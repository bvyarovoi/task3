package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/redis/go-redis/v9"
)

type CatFact struct {
	Text      string `json:"text"`
	UpdatedAt string `json:"updatedAt"`
	Type      string `json:"type"`
	CreatedAt string `json:"createdAt"`
}

var client *http.Client
var redisClient *redis.Client
var port string

func readEnvs() {
	port = os.Getenv("HTTP_PORT")
	if port == "" {
		port = "8080"
	}
}

func createClients() {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     "redis-master:6379",
		Password: "",
		DB:       0,
	})
	_, err := redisClient.Ping(context.Background()).Result()
	if err != nil {
		log.Fatal(err)
	}

	client = &http.Client{Timeout: 3 * time.Second}
}

func cats(w http.ResponseWriter, req *http.Request) {
	r, err := client.Get("https://cat-fact.herokuapp.com/facts")
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var facts []CatFact
	err = json.Unmarshal(body, &facts)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(r.StatusCode)

	randomIndex := rand.Intn(len(facts))
	randomFact := facts[randomIndex]

	fmt.Fprintf(w, "%s\n", randomFact.Text)
}

func counter(w http.ResponseWriter, req *http.Request) {

	count, err := redisClient.Get(context.Background(), "counter").Int()
	if err != nil {
		count = 0
	}
	count++
	redisClient.Set(context.Background(), "counter", count, 0).Result()

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "You are the %d-th visitor!\n", count)
}

func ok(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func main() {
	readEnvs()
	createClients()

	http.HandleFunc("/cats", cats)
	http.HandleFunc("/counter", counter)
	http.HandleFunc("/health", ok)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
