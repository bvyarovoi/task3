## Install

helm dependency update .helm  
helm upgrade --install app .helm  
  
Get deployed endpoints: `./get_urls.sh`  

## Endpoints

- /cats - get awesome fact about cats!
- /counter - visiting stats

## Update & deploy

Just commit your changes, CI/CD create a new image and upgrade helm release   
